import itertools
import json
import requests


_MBTA_VEHCILES = 'http://restbus.info/api/agencies/mbta/vehicles'
_MBTA_ROUTE_VEHICLES = 'http://restbus.info/api/agencies/mbta/routes/%s/vehicles'
_COMMAND = 'list'


def add_command(subparsers, command_handlers):
    parser = subparsers.add_parser(_COMMAND, help='List all buses')
    parser.add_argument('-r', '--route', help='List all bus ids of this route')
    command_handlers[_COMMAND] = get_bus_list


def fetch_bus_list(log, route=None):
    url = _MBTA_ROUTE_VEHICLES % route if route else _MBTA_VEHCILES
    log.debug('Fetching vehicle list from %s' % url)
    r = requests.get(url)
    all_vehicles = sorted(r.json(), key=lambda x: x['routeId'])
    vehicles_list = {}
    for routeid, vehicles in itertools.groupby(all_vehicles, bus_key_func):
        if routeid not in vehicles_list:
            vehicles_list[routeid] = []
        route_info = vehicles_list[routeid]

        for vehicle in vehicles:
            route_info.append(vehicle['id'])

    return vehicles_list

def get_bus_list(log, opts):
    buses = fetch_bus_list(log, opts.route)
    for routeid in buses:
        print ('\n', routeid, ':', end='')
        vehicles = buses[routeid]
        for vehicle in vehicles:
            print (vehicle, end=',')


def bus_key_func(businfo):
    return businfo['routeId']
