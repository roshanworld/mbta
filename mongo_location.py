import argparse
import pymongo

from datetime import datetime

_mongo_connection = None
_mongo_host = None
_mongo_port = None
def get_mongo_connection():
    global _mongo_connection, _mongo_host, _mongo_port
    if _mongo_connection is None:
        try:
            _mongo_connection = pymongo.MongoClient(_mongo_host,
                                                    _mongo_port)
        except:
            pass
    return _mongo_connection


def get_bus_db():
    conn = get_mongo_connection()
    return conn.mbta

def get_bus_locations_table():
    busdb = get_bus_db()
    bus_locations_table = busdb.bus_locations
    bus_locations_table.create_index([("busid", pymongo.ASCENDING),
                                      ("time", pymongo.DESCENDING)],
                                     unique=True,
                                     name="bustable_unique_index",
                                     background=True)
    return bus_locations_table

def add_bus_location(log, busid, routeid, lat, lon, direction, time):
    bus_locations_table = get_bus_locations_table()
    bus_location_entry = {
        'busid': busid,
        'routeid': routeid,
        'lat': lat,
        'lon': lon,
        'direction': direction,
        'time': time
    }
    try:
        bus_locations_table.insert_one(bus_location_entry)
        return bus_location_entry
    except pymongo.errors.DuplicateKeyError as e:
        log.error("Duplicate entry for combination busid:%s time:%s",
                  busid, time)
        return None

def get_bus_locations(busid, frm=datetime.min, until=datetime.now()):
    bus_locations_table = get_bus_locations_table()
    query = {
        'busid': busid,
        'time': {'$gte': frm},
        'time': {'$lte': until}
    }
    return bus_locations_table.find(query)

def get_all_locations():
    bus_locations_table = get_bus_locations_table()
    return bus_locations_table.find()





if __name__ == '__main__':
    sys.exit(main())
