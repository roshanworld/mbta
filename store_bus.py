import avro.io
import avro.schema
import itertools
import io
import json
import requests
import time

from kafka import SimpleProducer, KafkaClient

from bus_detail import fetch_bus_status

import mongo_location

_COMMAND = 'store'

def add_command(subparsers, command_handlers):
    parser = subparsers.add_parser(_COMMAND, help='List all buses')
    parser.add_argument('-b', '--busid', help='Store locations of this bus')
    parser.add_argument('-n', '--num', type=int, default=1,
                        help='Store <num> locations of this bus. If 0 then keep storing until bus entry is not found. If -1, stop storing if bus is not associated with any routeid')
    parser.add_argument('-k', '--kafka', help='Kafka Server <host>:<port> Default=%(default)s', default='localhost:9092')
    command_handlers[_COMMAND] = store_bus_location


def save_bus_location(log, busid, num, producer, schema):
    n = num
    rid = None
    while(num <= 0 or n > 0):
        routes = fetch_bus_status(log, busid=busid)
        if not routes:
            log.info("No information retrieved for busid %s", busid)
            break
        for routeid in routes:
            rid = routeid
            if num == -1 and rid == None:
                log.info('Bus not running for any route')
                num = 1
                break
            bus_locations = routes[routeid]
            for bus_location in bus_locations:
                bus_db_info = mongo_location.add_bus_location(
                    log,
                    bus_location['id'],
                    routeid,
                    bus_location['lat'],
                    bus_location['lon'],
                    bus_location['direction'],
                    bus_location['time'])
                if bus_db_info is not None:
                    bus_db_info['time'] = str(bus_db_info['time'])
                    bus_db_info['_id'] = str(bus_db_info['_id'])
                    producer.send_messages('vehicle_event',
                                           create_avro_message(bus_db_info,
                                                               schema))
                    n-=1
                else:
                    time.sleep(1)
    if rid:
        log.info("Inserted %s entries for bus %s route %s",
                 num, busid, rid)

def create_avro_message(json_data, schema):
    writer = avro.io.DatumWriter(schema)
    bytes_writer = io.BytesIO()
    encoder = avro.io.BinaryEncoder(bytes_writer)
    writer.write(json_data, encoder)
    #print(validate(schema, json_data))
    raw_bytes = bytes_writer.getvalue()
    return raw_bytes

def validate(expected_schema, datum):
  """Determine if a python datum is an instance of a schema."""
  INT_MIN_VALUE = -(1 << 31)
  INT_MAX_VALUE = (1 << 31) - 1
  LONG_MIN_VALUE = -(1 << 63)
  LONG_MAX_VALUE = (1 << 63) - 1

  schema_type = expected_schema.type
  print ('%s:%s:%s' % (schema_type, type(datum), expected_schema.name))
  if schema_type == 'null':
    return datum is None
  elif schema_type == 'boolean':
    return isinstance(datum, bool)
  elif schema_type == 'string':
    return isinstance(datum, str)
  elif schema_type == 'bytes':
    return isinstance(datum, bytes)
  elif schema_type == 'int':
    return (isinstance(datum, int)
        and (INT_MIN_VALUE <= datum <= INT_MAX_VALUE))
  elif schema_type == 'long':
    return (isinstance(datum, int)
        and (LONG_MIN_VALUE <= datum <= LONG_MAX_VALUE))
  elif schema_type in ['float', 'double']:
    return (isinstance(datum, int) or isinstance(datum, float))
  elif schema_type == 'fixed':
    return isinstance(datum, bytes) and (len(datum) == expected_schema.size)
  elif schema_type == 'enum':
    return datum in expected_schema.symbols
  elif schema_type == 'array':
    return (isinstance(datum, list)
        and all(Validate(expected_schema.items, item) for item in datum))
  elif schema_type == 'map':
    return (isinstance(datum, dict)
        and all(isinstance(key, str) for key in datum.keys())
        and all(Validate(expected_schema.values, value)
                for value in datum.values()))
  elif schema_type in ['union', 'error_union']:
    return any(Validate(union_branch, datum)
               for union_branch in expected_schema.schemas)
  elif schema_type in ['record', 'error', 'request']:
    print ([(field.name, datum.get(field.name))  for field in expected_schema.fields])
    return (isinstance(datum, dict)
        and all(validate(field.type, datum.get(field.name))
                for field in expected_schema.fields))
  else:
    print ('Unknown schema')


def store_bus_location(log, opts):
    producer = SimpleProducer(KafkaClient(opts.kafka))
    schema_path = 'mbta_message.avsc'
    schema = avro.schema.Parse(open(schema_path).read())
    save_bus_location(log, opts.busid, opts.num, producer, schema)
