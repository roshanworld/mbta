import requests
import sys
import time
import traceback


from copy2postgres import (GpsData, create_session)

_GEOCODER_URL = 'http://172.16.78.215:9000/GetAddress/%(lat)s/%(lon)s'
def update_addresses(db_session, gps_data_list):
    max_len = 0
    for gps_data in gps_data_list:
        location_str = '%(lat)f, %(lon)f' % {'lat': gps_data.latitude,
                                         'lon': gps_data.longitude}
        try:
            if gps_data.address:
                geocoder_resp = requests.get(_GEOCODER_URL % {'lat': gps_data.latitude,
                                                              'lon': gps_data.longitude})
                if geocoder_resp.status_code == 200:
                    location = geocoder_resp.json()
                    print (location)
                    gps_data.address = location['address']
                    print ('Updating address %s' % gps_data.address)
                    db_session.add(gps_data)
                    db_session.commit()
                    print ('Updated address for %s to %s' % (location_str, location['address']))
                else:
                    print (geocoder_resp)
                time.sleep(1.1)
            else:
                print(gps_data)
        except:
            traceback.print_exc()
            break

    print (max_len)

def main():
    db_session = create_session()
    gps_data_list = db_session.query(GpsData).order_by(GpsData.acquisition_time)
    update_addresses(db_session, gps_data_list)

if __name__ == '__main__':
    sys.exit(main())
