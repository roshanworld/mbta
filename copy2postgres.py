import requests
import sqlalchemy.orm.exc
import sys

from geopy.distance import great_circle
from sqlalchemy import create_engine, Column, Date, DateTime, Float, Integer, String, Sequence, Time
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

import mongo_location

_MIN_MOVE = 10

_PSYCOPG2_FOREIGN_KEY_VIOLATION_ERROR = '23503'
_PSYCOPG2_UNIQUE_KEY_VIOLATION_ERROR = '23505'
_GEOCODER_URL = 'http://172.16.78.111:9000/GetAddress/%(lat)s/%(lon)s'

Base = declarative_base()

class GpsData(Base):
    __tablename__ = 'gps_data'
    __table_args__ = ({'schema':'main'})
    id = Column(Integer, primary_key=True)
    gps_sensor_id = Column(Integer)
    utc_date = Column(Date)
    utc_time = Column(Time)
    acquisition_time = Column(DateTime)
    latitude = Column(Float)
    longitude = Column(Float)
    address = Column(String)
    height = Column(Float)
    speed = Column(Float)
    direction = Column(Float)

class GPSSensors(Base):
    __tablename__ = 'gps_sensors'
    __table_args__ = ({'schema':'main'})
    id = Column(Integer, primary_key=True)
    model_id = Column(Integer)
    frequency = Column(Float)
    sim = Column(String)
    imei = Column(String)

def add_sensor_data(conn, location):
    sensor_data = GPSSensors(
        imei=location['routeid'],
        model_id=1,
        sim=location['routeid']
    )
    conn.add(sensor_data)
    conn.commit()

def get_address(location):
    location_str = '%(lat)f, %(lon)f' % location
    geocoder_resp = requests.get(_GEOCODER_URL % location)
    if geocoder_resp.status_code == 200:
        loc = geocoder_resp.json()
        return loc['address']
    return None


def add_gps_data(conn, location):
    sensor = conn.query(GPSSensors).filter(GPSSensors.imei==location['routeid']).one()
    gps_data = GpsData(
        gps_sensor_id=sensor.id,
        acquisition_time=location['time'],
        latitude=location['lat'],
        longitude=location['lon'],
        direction=location['direction'],
        address=get_address(location)
    )
    conn.add(gps_data)
    conn.commit()

def create_session(host='localhost', db='test_db2', user='builder', passwd='pass'):
    db_engine = create_engine('postgresql+psycopg2://%(user)s:%(passwd)s@%(host)s/%(db)s'
                              % {'host': host,
                                 'db': db,
                                 'user': user,
                                 'passwd': passwd}
                             )
    Session = sessionmaker(bind=db_engine)
    db_session = Session()
    return db_session

def validate_move(last_location, current_location):
    if (last_location and last_location['routeid']==current_location['routeid']):
        last = (last_location['lat'], last_location['lon'])
        curr = (current_location['lat'], current_location['lon'])
        if great_circle(last, curr).meters < _MIN_MOVE:
            return False
    return True

def main():
    db_session = create_session()
    mong_bus_locations = mongo_location.get_all_locations()
    last_location = None
    for location in mong_bus_locations:
        try:
            if validate_move(last_location, location):
                add_gps_data(db_session, location)

            else:
                print('Skipping due to little movement')
            last_location = location
        except IntegrityError as e:
            db_session.rollback()
            if e.orig.pgcode == _PSYCOPG2_FOREIGN_KEY_VIOLATION_ERROR:
                print('Foreign Key Violation %s' % location['routeid'])
                add_sensor_data(db_session, location)
                add_gps_data(db_session, location)
            elif e.orig.pgcode == _PSYCOPG2_UNIQUE_KEY_VIOLATION_ERROR:
                print('Duplicate Entry %s' % location)
            else:
                print(e)
                print(e.orig.pgcode)
                break
        except sqlalchemy.orm.exc.NoResultFound as e:
            db_session.rollback()
            print('Foreign Key Violation %s' % location['routeid'])
            add_sensor_data(db_session, location)
            add_gps_data(db_session, location)
    db_session.close()

if __name__ == '__main__':
    sys.exit(main())
