import argparse
import json
import logging
import requests
import sys

import bus_detail
import list_bus
import store_bus

_MBTA_BASE_URL = 'http://restbus.info/api/agencies/mbta/'

_COMMANDS = {}

def parse_args():
    arg_parser = argparse.ArgumentParser(description='MBTA Library')
    arg_parser.add_argument('-v', '--verbose', action='store_true',
                            help='Verbose output')
    subparsers = arg_parser.add_subparsers(help='Available commands', dest='command')

    list_bus.add_command(subparsers, _COMMANDS)
    bus_detail.add_command(subparsers, _COMMANDS)
    store_bus.add_command(subparsers, _COMMANDS)


    return arg_parser.parse_args()

def main():
    opts = parse_args()
    logging.basicConfig(level=logging.DEBUG)
    _COMMANDS[opts.command](logging, opts)


if __name__=='__main__':
    sys.exit(main())
