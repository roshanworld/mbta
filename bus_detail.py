import itertools
import json
import requests

from datetime import datetime, timedelta

_MBTA_VEHICLE_INFO = 'http://restbus.info/api/agencies/mbta/vehicles/%s'
_MBTA_VEHICLES = 'http://restbus.info/api/agencies/mbta/vehicles'
_MBTA_ROUTE_VEHICLES = 'http://restbus.info/api/agencies/mbta/routes/%s/vehicles'
_COMMAND = 'status'

def add_command(subparsers, command_handlers):
    parser = subparsers.add_parser(_COMMAND, help='Get Status of buses')
    mte_group = parser.add_mutually_exclusive_group()
    mte_group.add_argument('-b', '--busid',
                           help='List status of this bus')
    mte_group.add_argument('-r', '--route',
                           help='List status of all buses in this route')
    command_handlers[_COMMAND] = get_bus_status


def get_query_url(busid, route):
    if busid:
        return _MBTA_VEHICLE_INFO % busid
    if route:
        return _MBTA_ROUTE_VEHICLES % route
    return _MBTA_VEHICLES


def fetch_bus_status(log, busid=None, routeid=None):
    url = get_query_url(busid, routeid)
    log.debug('Fetching vehicle list from %s' % url)
    r = requests.get(url)
    response_time = r.headers['last-modified']
    response_time = datetime.strptime(response_time, '%a, %d %b %Y %H:%M:%S %Z')
    if r.status_code != 200:
        log.error("Error in getting data: %s", r.json())
        return None
    all_vehicles = r.json()
    all_vehicles = all_vehicles if type(all_vehicles) == list else [all_vehicles]
    all_vehicles = sorted(all_vehicles, key=lambda x: x['routeId'])
    vehicles_info = {}
    for routeid, vehicles in itertools.groupby(all_vehicles, bus_key_func):
        if routeid not in vehicles_info:
            vehicles_info[routeid] = []
        route_info = vehicles_info[routeid]
        for vehicle in vehicles:
            vehicle_info = {
                'id': vehicle['id'],
                'lat': vehicle['lat'],
                'lon': vehicle['lon'],
                'direction': vehicle['heading'],
                'time': (response_time -
                         timedelta(seconds=vehicle['secsSinceReport']))}
            route_info.append(vehicle_info)
    return vehicles_info


def get_bus_status(log, opts):
    buses = fetch_bus_status(log, opts.busid, opts.route)
    for routeid in buses:
        vehicles = buses[routeid]
        print ('\n', routeid, ':', end='')
        for vehicle in vehicles:
            print ('\t', vehicle['id'], ',', vehicle['lat'],
                   ',', vehicle['lon'], ',', vehicle['direction'],
                   ',', vehicle['time'])


def bus_key_func(businfo):
    return businfo['routeId']
